#!/usr/bin/env python

"""Tests for `hex_change_detection_ndvi` package."""
import homura
import json
import os
import pytest
import shutil
import tempfile

from hex_change_detection_ndvi.gdal_utils import GdalUtils


@pytest.fixture
def geometry():
    """ Geometry fixture for tests """
    geojson_mock_data = {
        "type": "FeatureCollection",
        "crs": {
            "type": "name",
            "properties": {"name": "urn:ogc:def:crs:EPSG::32622"},
        },
        "features": [{
                "type": "Feature",
                "geometry": {
                    "type": "MultiPolygon",
                    "coordinates": [[[
                        [577102.816966289537959, -1855367.753468998475],
                        [682380.987676345044747, -1858187.704470160650089],
                        [682380.987676345044747, -1858187.704470160650089],
                        [679091.044841655762866, -1955006.022176729515195],
                        [571932.906797492178157, -1952656.063009094446898],
                        [577102.816966289537959, -1855367.753468998475]
                    ]]]
                }
            }
        ]
    }

    output_path = os.path.join(tempfile._get_default_tempdir(), "saida.json")
    if os.path.exists(output_path):
        return output_path

    with open(output_path, "w") as f:
        f.write(json.dumps(geojson_mock_data))

    return output_path


@pytest.fixture
def image():
    """ Image fixture for tests that downloads data from download_link """

    download_link = "http://landsat-pds.s3.amazonaws.com/" \
        "c1/L8/222/072/LC08_L1TP_222072_20200925_20200925_01_RT/" \
        "LC08_L1TP_222072_20200925_20200925_01_RT_BQA.TIF"
    output_path = os.path.join(tempfile._get_default_tempdir(), "saida.TIF")

    if os.path.exists(output_path):
        return output_path

    try:
        homura.download(download_link, path=output_path)
    except Exception as exc:
        print("\n\n{}\n\n".format(exc))
        return False

    return output_path


def test_creation_temporary_dir():
    """Sample pytest test function to test temporary dir name"""

    tempdir = GdalUtils.get_temp_output_directory()
    assert tempdir
    assert "/tmp" in tempdir


def test_creation_dir():
    """Sample pytest test function to test dir creation"""

    tempdir = os.path.join(
        tempfile._get_default_tempdir(),
        next(tempfile._get_candidate_names())
    )
    tempdir = GdalUtils.check_dir_exists(tempdir)
    assert tempdir
    assert os.path.isdir(tempdir)
    shutil.rmtree(tempdir)

    tempdir = os.path.join(
        tempfile._get_default_tempdir(),
        next(tempfile._get_candidate_names())
    )
    assert not os.path.exists(tempdir)
    assert not os.path.isdir(tempdir)
    tempdir = GdalUtils.check_dir_exists(tempdir)
    assert tempdir
    shutil.rmtree(tempdir)


def test_geometry_cutline(geometry, image):
    assert geometry
    assert image
    assert os.path.exists(geometry)
    assert os.path.exists(image)

    cutline = GdalUtils.cutline(geometry, image)
    assert cutline
    assert ".TIF" in cutline or ".tif" in cutline
    assert os.path.exists(cutline)
