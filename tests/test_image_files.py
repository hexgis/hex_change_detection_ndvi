#!/usr/bin/env python

"""Tests for `hex_change_detection_ndvi` package."""
import os
import sys
import pytest

from hex_change_detection_ndvi.image_files import ImageFiles

if not os.getenv('CHANGE_DETECTION_TOKEN_AUTH'):
    sys.exit("Please define env CHANGE_DETECTION_TOKEN_AUTH for testing"
             "\nExample: export CHANGE_DETECTION_TOKEN_AUTH=Token\\ secret")


@pytest.fixture
def get_images():
    """ Return product for t0 and t1 for ImageFiles tests """
    return {
        "t0": "LC08_L1TP_222072_20200925_20200925_01_RT",
        "t1": "LC08_L1TP_222072_20200925_20200925_01_RT"
    }


@pytest.fixture
def get_band_names():
    """ Return band names list for ImageFiles tests """
    return ['red1', 'red2', 'near_infrared1', 'near_infrared2']


def test_image_files_request_success(get_images, get_band_names):
    """Sample pytest test for ImageFiles creation data"""
    images = ImageFiles(
        image_t0=get_images.get("t0"),
        image_t1=get_images.get("t1"),
        request_token=os.getenv("CHANGE_DETECTION_TOKEN_AUTH"),
        band_names=get_band_names
    )
    assert images


def test_image_files_request_error(get_images, get_band_names):
    """Sample pytest test for ImageFiles creation data"""
    with pytest.raises(Exception):
        ImageFiles(
            image_t0=get_images.get("t0"),
            image_t1=get_images.get("t1"),
            request_token=None,
            band_names=get_band_names
        )
