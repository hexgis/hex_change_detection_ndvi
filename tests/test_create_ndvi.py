#!/usr/bin/env python

"""Tests for `hex_change_detection_ndvi` package."""
import homura
import os
import pytest
import tempfile

from hex_change_detection_ndvi.create_normalized_difference import (
    NormalizedDifference
)


@pytest.fixture
def image_b5():
    """ Image fixture for tests that downloads data from download_link """

    download_link = "http://landsat-pds.s3.amazonaws.com/" \
        "c1/L8/222/072/LC08_L1TP_222072_20200925_20200925_01_RT/" \
        "LC08_L1TP_222072_20200925_20200925_01_RT_B5.TIF"
    output_path = os.path.join(tempfile._get_default_tempdir(), "nir.tif")

    if os.path.exists(output_path):
        return output_path

    try:
        homura.download(download_link, path=output_path)
    except Exception as exc:
        print("\n\n{}\n\n".format(exc))
        return False

    return output_path


@pytest.fixture
def image_b4():
    """ Image fixture for tests that downloads data from download_link """

    download_link = "http://landsat-pds.s3.amazonaws.com/" \
        "c1/L8/222/072/LC08_L1TP_222072_20200925_20200925_01_RT/" \
        "LC08_L1TP_222072_20200925_20200925_01_RT_B4.TIF"
    output_path = os.path.join(tempfile._get_default_tempdir(), "red.tif")

    if os.path.exists(output_path):
        return output_path

    try:
        homura.download(download_link, path=output_path)
    except Exception as exc:
        print("\n\n{}\n\n".format(exc))
        return False

    return output_path


def test_image_download(image_b4, image_b5):
    """Sample pytest test for downloaded band"""

    assert os.path.exists(image_b4)
    assert os.path.exists(image_b5)


def test_ndvi_exception_for_image_error():
    """Sample pytest test for NDVI image error"""
    with pytest.raises(TypeError):
        NormalizedDifference()

    with pytest.raises(AttributeError):
        error_img = '/tmp/test_error.tif'
        NormalizedDifference(band_1=error_img, band_2=error_img)


def test_ndvi_creation_success(image_b4, image_b5):
    """Sample pytest test for NDVI image success creation"""
    ndvi = NormalizedDifference(band_1=image_b4, band_2=image_b5)
    assert ndvi.dataset is not None
    assert ndvi.save_raster(output_path="/tmp/ndvi.tif")
    assert os.path.exists("/tmp/ndvi.tif")
