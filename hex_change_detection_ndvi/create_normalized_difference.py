import numpy as np

from osgeo import gdal

from .gdal_utils import GdalUtils


class NormalizedDifference:
    """
    Calculate Normalized Difference Index for images under Predefined Formula
    BAND_2 - BAND_1 / BAND_2 + BAND_1

    Creates atmospheric correction raster when it is selected
    Using sun_elevation data from image metadata.

    Able to create indexes for:
        * Normalized Difference Vegetation Index (NDVI)
        * Normalized Difference Water Index (NDWI)
        * Normalized Difference Moisture Index (NDMI)
        * Normalized Difference Glacier Index (NDGI)
        * Normalized Difference Snow Index (NDSI)
        * Normalized Pigment Chlorophyll Ratio Index (NPCRI)
        * Normalized Burn Ratio calculation (NBR)

    Arguments:
        * band_1 (string): path to band_1 image
        * band_2 (string): path to band_2 image
        * sun_elevation (float): sun elevation data
        * correct (boolean): apply atmospheric correction to image
        * quiet (boolean): verbose logs
    """

    def __init__(
        self,
        band_1,
        band_2,
        sun_elevation=False,
        correct=False,
        quiet=False
    ):
        """ Normalized Index calc for images using numpy """
        if correct:
            self.sun_elevation = sun_elevation

        self.quiet = quiet
        self.correct = correct
        self.band_1 = gdal.Open(band_1)
        self.band_2 = gdal.Open(band_2)
        self.dataset = self.create_nindex_array()

    def save_raster(
        self,
        output_path,
        dataset=None
    ):
        """
        Method to save raster from a dataset to a output path raster tiff
        Delete data from OS HDD and memory when process return  data

        Arguments:
            * dataset (GdalBand): GdalBand data.
                If it is none, will assume to create dataset from self.dataset
            * output_path (String): Dataset output path

        Returns:
            * output_path (String): Dataset output path
        """

        if not self.quiet:
            print("Saving raster to {}".format(output_path))

        if dataset is None:
            dataset = self.dataset

        raster_set = gdal.GetDriverByName('GTiff').Create(
            output_path,
            self.band_1.RasterXSize,
            self.band_1.RasterYSize,
            1,
            gdal.GDT_Float32
        )

        projection = GdalUtils.get_projection(self.band_1, self.quiet)
        geotransform = GdalUtils.get_geo_transform(self.band_1, self.quiet)

        raster_set.SetProjection(projection)
        raster_set.SetGeoTransform(geotransform)

        raster_set.GetRasterBand(1).WriteArray(dataset)
        raster_set.GetRasterBand(1).SetNoDataValue(-999)
        raster_set = None

        if not self.quiet:
            print("Raster created\n")

        return output_path

    def create_nindex_array(self):
        """
        Method to create normalized index data for image array

        Returns:
            * normalized_data (npArray): Dataset normalized_data
        """
        if not self.quiet:
            print("Creating NDVI with band_1 and band_2 bands..."
                  "\nParameter: correct data = {}".format(self.correct))

        band_1 = GdalUtils.get_raster_band_as_array(
            self.band_1, quiet=self.quiet)
        band_2 = GdalUtils.get_raster_band_as_array(
            self.band_2, quiet=self.quiet)

        if self.correct:
            if not self.quiet:
                print("Creating NIR and band_1 with reflectance values...")

            band_1_ref = np.divide(
                np.multiply(band_1, 0.0002) - 0.1,
                np.sin(self.sun_elevation),
                where=(band_1 != 0)
            )

            band_2_ref = np.divide(
                np.multiply(band_2, 0.0002) - 0.1,
                np.sin(self.sun_elevation),
                where=(band_2 != 0)
            )

            nindex = np.divide(
                band_2_ref - band_1_ref,
                band_2_ref + band_1_ref,
                where=(band_2_ref - band_1_ref) != 0
            )

        else:
            nindex = np.divide(
                band_2 - band_1,
                band_2 + band_1,
                where=(band_2 - band_1) != 0
            )

        if not self.quiet:
            print("Done!\n")

        nindex[nindex == 0] = -999
        return nindex
