#!/usr/local/bin/python3
import os
import numpy as np

from datetime import datetime
from osgeo import ogr, gdal, osr
from argparse import ArgumentParser

from hex_change_detection_ndvi.create_normalized_difference import (
    NormalizedDifference
)


class NdviDifference:
    """
    Calculate NDVI with diff images. Assumes atmospheric correction.
    """

    def __init__(
        self,
        red_1,
        near_infrared_1,
        red_2,
        near_infrared_2,
        output_path,
        contour=False,
        save_intermediate_files=False,
        contour_base=0.1,
        sun_elevation=False,
        correct=False,
        quiet=False
    ):
        if contour_base is None:
            self.contour_base = 0.1
        else:
            self.contour_base = contour_base

        if correct:
            self.sun_elevation = sun_elevation

        self.quiet = quiet
        self.correct = correct
        self.contour = contour
        self.output_path = output_path
        self.save_intermediate_files = save_intermediate_files

        self.red_1 = red_1
        self.near_infrared_1 = near_infrared_1
        self.red_2 = red_2
        self.near_infrared_2 = near_infrared_2

        # Running ndvi difference
        self.outputs = self.main()

    def get_gdal_driver_by_name(self, driver_name="ESRI Shapefile"):
        return ogr.GetDriverByName(driver_name)

    def create_polylines(
            self,
            input_path,
            output_path,
            output_name='lines',
            contour_base=0.01
        ):
        """
        Method to create contour lines from input dataset

        Arguments:
            * input_path (String): path for raster image
            * output_path (String): output_path path for raster image
            * contour_base (Float): value to contour base
            * quiet (Boolean): run quietly
        """

        if not self.quiet:
            print("Creating polylines from {} to {}".format(
                input_path, output_path))

        ogr_datasource = self.get_gdal_driver_by_name()
        ogr_datasource = ogr_datasource.CreateDataSource(output_path)
        dataset_ndvi = gdal.Open(input_path)
        ndvi_raster = dataset_ndvi.GetRasterBand(1)
        projection = dataset_ndvi.GetProjectionRef()  # GetProjection()
        srs = osr.SpatialReference(wkt=projection)
        contour_shape = ogr_datasource.CreateLayer(output_name, srs)

        field_defn = ogr.FieldDefn("ID", ogr.OFTInteger)
        contour_shape.CreateField(field_defn)
        field_defn = ogr.FieldDefn("change", ogr.OFTReal)
        contour_shape.CreateField(field_defn)

        try:
            gdal.ContourGenerate(
                ndvi_raster, contour_base, 0,
                [], 1, -999,
                contour_shape, 0, 1
            )
            if not self.quiet:
                print("Contour created in {}".format(output_path))
        except Exception as exc:
            if not self.quiet:
                log = "Error while tryig to create contour for {}".format(
                    output_path)
                print(log)
            print(exc)
        ogr_datasource = None

    def get_ndvi_temp_names(self, output_path=None, tif=True):
        """
        Method to create file name for ndvi data with datetime

        Arguments:
            * output_path (String): output_path path for raster image
        """

        if not output_path:
            output_path = self.output_path
        return (
            os.path.join(output_path, "ndvi_1_{}".format(
                datetime.now().strftime('%Y%m%d_%H%M%S%f'))),
            os.path.join(output_path, "ndvi_2_{}".format(
                datetime.now().strftime('%Y%m%d_%H%M%S%f'))),
            os.path.join(output_path, "ndvi_diff_{}".format(
                datetime.now().strftime('%Y%m%d_%H%M%S%f'))),
        )

    def get_ndvi_changes(self, ndvi_1, ndvi_2):

        if not self.quiet:
            print("Creating ndvi data difference...")

        ndvi_change = ndvi_2 - ndvi_1
        return np.where(
            (ndvi_1 >- 999) & (ndvi_2 >- 999),
            ndvi_change,
            -999
        )

    def main(self):
        """ Main method that creates difference dataset """
        if self.correct:
            ndvi_1 = NormalizedDifference(
                band_1=self.red_1,
                band_2=self.near_infrared_1,
                correct=self.correct,
                sun_elevation=self.sun_elevation[0]
            )
            ndvi_2 = NormalizedDifference(
                band_1=self.red_2,
                band_2=self.near_infrared_2,
                correct=self.correct,
                sun_elevation=self.sun_elevation[1]
            )
        else:
            ndvi_1 = NormalizedDifference(
                band_1=self.red_1, band_2=self.near_infrared_1)
            ndvi_2 = NormalizedDifference(
                band_1=self.red_2, band_2=self.near_infrared_2)

        output_files = self.get_ndvi_temp_names()

        if self.save_intermediate_files:
            ndvi_1.save_raster(output_path=output_files[0] + ".TIF")
            ndvi_2.save_raster(output_path=output_files[1] + ".TIF")

        ndvi_change = self.get_ndvi_changes(ndvi_1.dataset, ndvi_2.dataset)

        output_path = output_files[2] + ".TIF"
        ndvi_1.save_raster(dataset=ndvi_change, output_path=output_path)

        if self.contour:
            self.create_polylines(
                output_files[2] + ".TIF",
                output_files[2],
                contour_base=self.contour_base
            )

            return output_files, output_files[2]

        return output_files


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument(
        "-r1", "--red-1",
        dest="red_1", required=True,
        help="Path to first red band",
        metavar="RED_BAND_1"
    )
    parser.add_argument(
        "-r2", "--red-2",
        dest="red_2", required=True,
        help="Path to second red band",
        metavar="RED_BAND_2"
    )
    parser.add_argument(
        "-n1", "--nir-1",
        dest="near_infrared_1", required=True,
        help="Path to first nir band",
        metavar="NIR_BAND_1"
    )
    parser.add_argument(
        "-n2", "--nir-2",
        dest="near_infrared_2", required=True,
        help="Path to second nir band",
        metavar="NIR_BAND_2"
    )
    parser.add_argument(
        "-base", "--countour_base",
        dest="contour_base", type=float,
        help="Contour value data",
        required=True,
        metavar="COUNTOUR_VALUE"
    )
    parser.add_argument(
        "-output", "--output-path",
        dest="output", type=str,
        help="output path to images",
        required=True,
        metavar="OUTPUT_PATH"
    )
    parser.add_argument(
        "-sun", "--sun-elevation",
        dest="sun_elevation",
        nargs='+', type=float,
        help="Sun elevation value",
        metavar="SUN_ELEVATION"
    )
    parser.add_argument(
        "-correct", "--correct-image",
        dest="correct", action="store_true",
        default=False,
        help="Correct toa reflectance images"
    )
    parser.add_argument(
        "-q", "--quiet",
        action="store_true",
        dest="quiet", default=False,
        help="don't print status messages to stdout"
    )
    parser.add_argument(
        "-c", "--contour", action="store_true",
        dest="contour", default=False,
        help="create contour polyline from difference"
    )

    args = parser.parse_args()
    NdviDifference(
        args.red_1, args.near_infrared_1,
        args.red_2, args.near_infrared_2,
        args.output, args.contour,
        args.contour_base, args.sun_elevation,
        args.correct, args.quiet)
