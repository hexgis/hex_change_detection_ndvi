#!/usr/bin/python3
import os
import urllib
import requests
import tempfile


class ImageFiles:
    """ Image files for Change Detection ndvi """
    def __init__(
        self,
        image_t0,
        image_t1,
        request_token,
        band_names,
        images_url='http://skynet.xskylab.com/landsat/scene/',
    ):
        """
        Image files class to return images for Change Detection process

        Arguments:
            * image_t0 (str): image t0 in timeline
            * image_t1 (str): image t1 in timeline
            * request_token (str): skynet api request token
            * band_names (list): bands list name.
                E.g.: ['red1', 'red2', 'near_infrared1', 'near_infrared2']
            * images_url (str): Landsat scenes and images url.
                E.g.: http://skynet.xskylab.com/landsat/scene/
        """
        self.image_t0 = image_t0
        self.image_t1 = image_t1
        self.image_path = self.__get_tmp_images_path()
        self.images_url = images_url
        self.__request_token = request_token
        self.__band_names = band_names

        self.images = {
            self.__band_names[0]: '',
            self.__band_names[1]: '',
            self.__band_names[2]: '',
            self.__band_names[3]: ''
        }

        self.__download_images()

    def __get_tmp_images_path(self, suffix="_ndvi_cd"):
        """
        Void Method to create and return temporary dir for NDVI process

        Arguments:
            * suffix (str): suffix for temp

        Returns:
            * name (str): temporary path
        """
        return os.path.join(
            tempfile.gettempdir(),
            next(tempfile._get_candidate_names()) + suffix
        )

    def __check_creation_folder(self, path):
        """
        Check and create folder path Will always returns path

        Arguments:
            * path (str): path to check creation

        Returns:
            * name (str): path to created folder
        """
        if os.path.exists(path):
            return path
        return os.makedirs(path)

    def __download_images(self):
        """
        *Method to create result folders and read the json file*

        Uses images names to get bands to download
        """
        self.__input_path = os.path.join(self.image_path, 'input')
        self.__result_path = os.path.join(self.image_path, 'results')
        self.__check_creation_folder(self.__input_path)
        self.__check_creation_folder(self.__result_path)

        self.__download_image_bands(self.image_t0, image_order="1")
        self.__download_image_bands(self.image_t1, image_order="2")

    def __request_data(self, url, headers):
        """
        Private method to get url and headers and request data
        Uses requests to get data using argument headers and return json data

        Arguments:
            * url (str): url to request data
            * headers (object): headers objects

        Raises:
            * Exception: User request error

        Returns:
            * data (json): json object
        """
        request = requests.get(url, headers=headers)
        if not request.ok:
            log = "Error while getting request data from API" \
                "Url={}, Response={}, Status code: {}".format(
                    url, request.json(), request.status_code)
            raise Exception(log)

        return request.json()

    def __download_image_bands(
        self,
        image_name,
        image_order
    ):
        """
        Method to download band of image calling a request to
        skynet API, building the array of bands path.

        Arguments:
            * image_name (json): image name on json structure
            * image_order (str): fixed number of images needed to build a ndvi
                difference (old and recent)
        """
        try:
            request_headers = {'Authorization': self.__request_token}
            img_url = urllib.parse.urljoin(self.images_url, image_name)
            response = self.__request_data(img_url, request_headers)
            images = response['properties']['images']

            red_prefix = self.__band_names[0][0:-1]
            nir_prefix = self.__band_names[2][0:-1]

            red = [obj for obj in images if(obj['type'] == 'B4')]
            if red:
                self.images[red_prefix + image_order] = red[0]['identifier']
                req = os.path.join(self.__input_path, red[0]['identifier'])
                urllib.request.urlretrieve(red[0]['download_url'], req)

            nir = [obj for obj in images if(obj['type'] == 'B5')]
            if nir:
                self.images[nir_prefix + image_order] = nir[0]['identifier']
                req = os.path.join(self.__input_path, nir[0]['identifier'])
                urllib.request.urlretrieve(nir[0]['download_url'], req)

        except Exception as exc:
            log = "Error while trying do download image bands - {}"
            raise Exception(log.format(exc))

    def get_images(self):
        """
        Method to get the array with image bands

        Returns:
            * images (string array): array with image bands path
        """
        try:
            if self.images:
                return self.images
            return None
        except Exception as exc:
            print('Error trying do get images {}'.format(exc))

    def get_input_path(self):
        """
        Method to get input folder path

        Returns:
            * input_path (string): input folder containing image bands
        """
        if self.__input_path:
            return self.__input_path
        return None

    def get_intersection_path(self):
        """
        Method to get the path containing band intersection shapefile

        Returns:
            * intersection_folder (string): path to intersection folder
        """
        if self.__intersection_folder:
            return self.__intersection_folder
        return None

    def get_cutted_bands_path(self):
        """
        Method to get the image bands cutted by intersection folder

        Returns:
            * cutted_bands_folder (string): folder of cutted rasters
        """
        if self.__cutted_bands_folder:
            return self.__cutted_bands_folder
        return None

    def get_output_path(self):
        """
        Method to get output of ndvi geometries and rasters folder

        Returns:
            * output_folder (string): path of results folder
        """
        if self.__output_folder:
            return self.__output_folder
        return None

    def create_result_inner_folders(self, image_op):
        """
        Void method to create image inner folders, containing
        folder of intersection, cutted bands and output.

        Arguments:
            * image_op (string): orbit point number of image
        """
        self.__image_folder = os.path.join(
            self.__result_path, image_op)
        self.__intersection_folder = os.path.join(
            self.__image_folder, "intersection")
        self.__cutted_bands_folder = os.path.join(
            self.__image_folder, "cutted")
        self.__output_folder = os.path.join(
            self.__image_folder, "output")

        self.__check_creation_folder(self.__image_folder)
        self.__check_creation_folder(self.__output_folder)
        self.__check_creation_folder(self.__cutted_bands_folder)
        self.__check_creation_folder(self.__intersection_folder)
