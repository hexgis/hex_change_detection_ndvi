#!/usr/bin/python3
import os

from argparse import ArgumentParser

from hex_change_detection_ndvi.image_files import ImageFiles
from hex_change_detection_ndvi.shape_intersection import ShapefileArea
from hex_change_detection_ndvi.process_ndvi_diff import NdviDifference
from hex_change_detection_ndvi.gdal_utils import GdalUtils


class GenerateChangeDetection:
    """ GenerateChangeDetection class """

    def __init__(
        self,
        image_t0,
        image_t1,
        request_token,
        band_names=None,
        images_url='http://skynet.xskylab.com/landsat/scene/',
    ):
        """
        Detect changes from NDVI difference between t0 and t1 images

        Arguments:
            * image_t0 (str): image t0 in timeline
            * image_t1 (str): image t1 in timeline
            * request_token (str): skynet api request token
            * band_names (list): bands list name.
                E.g.: ['red1', 'red2', 'near_infrared1', 'near_infrared2']
            * images_url (str): Landsat scenes and images url.
                E.g.: http://skynet.xskylab.com/landsat/scene/'
        """
        if not band_names:
            band_names = [
                'red1',
                'red2',
                'near_infrared1',
                'near_infrared2'
            ]
        else:
            band_names = band_names.split(',')

        self.create_polygons_based_on_json(
            band_names,
            image_t0,
            image_t1,
            images_url,
            request_token
        )

    def create_polygons_based_on_json(
        self,
        band_names,
        image_t0,
        image_t1,
        images_url,
        request_token
    ):
        """
        Creates polygons based on json data for landsat images

        Attributes data when difference finishes
        Attributes is_valid when process finishes

        Arguments:
            * band_names (list): list with names of bands
            * image_t0 (str): image t0 in timeline
            * image_t1 (str): image t1 in timeline
            * request_token (str): skynet api request token
            * band_names (list): bands list name
            * images_url (str): Landsat scenes and images url.
                E.g.: http://skynet.xskylab.com/landsat/scene/'
        """
        try:
            image_files = ImageFiles(
                image_t0=image_t0,
                image_t1=image_t1,
                images_url=images_url,
                request_token=request_token,
                band_names=band_names
            )
        except Exception as exc:
            raise ValueError("Error getting Images: {}".format(exc))

        images = image_files.get_images()
        image_op = images["red1"][10:16]
        image_files.create_result_inner_folders(image_op)
        input_path = image_files.get_input_path()
        intersection_path = image_files.get_intersection_path()
        cutted_bands_path = image_files.get_cutted_bands_path()
        output_path = image_files.get_output_path()

        intersection_file_name = 'intersection'

        intersection_area = ShapefileArea(
            os.path.join(input_path, images[band_names[0]]),
            os.path.join(input_path, images[band_names[1]]),
            [0, 0, 0],
            os.path.join(intersection_path, intersection_file_name)
        )

        intersection_area.transform_rasters()
        intersection_area.cut_images(
            images[band_names[0]],
            images[band_names[1]],
            images[band_names[2]],
            images[band_names[3]],
            input_path,
            intersection_path,
            cutted_bands_path,
            intersection_file_name
        )

        diffOut = NdviDifference(
            os.path.join(cutted_bands_path, 'r1.tif'),
            os.path.join(cutted_bands_path, 'n1.tif'),
            os.path.join(cutted_bands_path, 'r2.tif'),
            os.path.join(cutted_bands_path, 'n2.tif'),
            output_path,
            True,
            0.1
        )

        polygons = GdalUtils.create_polygons_from_lines(
            shapefile=os.path.join(diffOut.outputs[1], "lines.shp"),
            output_file_name="polygons.shp",
            output_path=diffOut.outputs[1])

        self.data = polygons
        self.is_valid = True


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument(
        "-b", "--bands",
        dest="image_bands", required=False,
        help="Image Bands Name (Write the names separated by a comma)",
        metavar="IMAGE_BANDS"
    )
    parser.add_argument(
        "-t0", "--image_t0",
        dest="image_t0", required=True,
        help="First image to diff",
        metavar="IMAGE_T0"
    )
    parser.add_argument(
        "-t1", "--image_t1",
        dest="image_t1", required=True,
        help="Last image to diff",
        metavar="IMAGE_T1"
    )
    parser.add_argument(
        "-u", "--url",
        dest="request_url", required=False,
        help="Request URL to image download",
        metavar="REQUEST_URL",
        default="http://skynet.xskylab.com/landsat/scene/"
    )
    parser.add_argument(
        "-t", "--token",
        dest="request_token", required=True,
        help="Authorization token to request",
        metavar="REQUEST_TOKEN"
    )

    args = parser.parse_args()

    GenerateChangeDetection(
        band_names=args.image_bands,
        image_t0=args.image_t0,
        image_t1=args.image_t1,
        request_token=args.request_token,
        images_url=args.request_url
    )
