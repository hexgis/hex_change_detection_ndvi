#!/usr/env/python3
import os
import subprocess
import tempfile

from argparse import ArgumentParser
from osgeo import ogr


class ShapefileArea:
    def __init__(
        self,
        input_file_path,
        input_file_path2,
        nodata=[0, 0, 0],
        output_file_name="intersection"
    ):
        """
        ShapefileArea class to transform raster data to a mask shapefile

        Creates a shapefile mask from a raster useful area to
        translate mask to a virtual one band data and
        polygonize useful area to get common area between two different rasters

        Arguments:
            * input_file_path (str): file path to raster
            * input_file_path2 (str): file path to second raster
            * output_file_name (str): outputfile name for shapefile
            * nodata (list): nodata values for raster, according to input file
                number of bands
        """
        self.input_file_path = input_file_path
        self.input_file_path2 = input_file_path2
        self.nodata = ",".join(map(str, nodata))
        self.output_file_name = output_file_name

    def __get_mask_command(self):
        """Private method to get mask command"""
        return "gdal_translate " + \
            "-mask 1 -b mask -of vrt " + \
            "-a_nodata {nodata} " + \
            "{tiffile} {output}"

    def __get_translated_data_command(self):
        """Private method to get translate data command"""
        return "gdal_translate " + \
            "-b 1 -of vrt -a_nodata 0 " + \
            "{input} {output}"

    def __get_polygonize_command(self):
        """Private method to get polygonize data command"""
        return "gdal_polygonize.py " + \
            "-8 {input} -b 1 " + \
            "-f 'ESRI Shapefile' " + \
            "{output}"

    def get_driver(self, driver_name="ESRI Shapefile"):
        """
        Method to get ogr driver for driver_name

        Arguments:
            * driverName (str): name of drive

        Returns:
            * driver (ogr.Driver): driver
        """
        return ogr.GetDriverByName(driver_name)

    def open_shapefile(self, shapefile, mode=0):
        """
        Method to open shapefile data with gdal

        Arguments:
            * shapefile (str): shapefile path
            * mode (int): open method mode
        Returns:
            * driver (ogr.DataSource): shapefile datasource
        """
        drive = self.get_driver()
        return drive.Open(shapefile, mode)

    def get_layer(self, dataset):
        """
        Method to get layer from shapefile with gdal

        Arguments:
            * dataset (ogr.DataSource): gdal dataset pointer
        Returns:
            * layer (ogr.Layer): layer from datasource
        """
        return dataset.GetLayer()

    def get_feature(self, layer, feature_number=0):
        """
        Method to get feature from shapefile layer with gdal

        Arguments:
            * layer (ogr.Layer): gdal layer pointer
            * feature_number (int): number of feature
        Returns:
            * feature (ogr.Feature): feature from layer
        """
        return layer.GetFeature(feature_number)

    def get_geometry(self, layer):
        """
        Method to get geometry from shapefile layer feature with gdal

        Arguments:
            * layer (ogr.Layer): gdal layer pointer
        Returns:
            * feature (ogr.Geometry): geometry data from feature
        """
        return layer.geometry()

    def get_intersection(self, shapefile, shapefile2):
        """
        Method to get intersection from shapefiles datasources
        Will check if geometries intersects and
        return geometry for intersected area

        Arguments:
            * shapefile (str): shapefile path
            * shapefile2 (str): shapefile path
        Returns:
            * intersection (ogr.Geometry): geometry intersection
        """
        dataset = self.open_shapefile(shapefile)
        dataset2 = self.open_shapefile(shapefile2)

        layer = self.get_layer(dataset)
        layer2 = self.get_layer(dataset2)

        feat = self.get_feature(layer)
        feat2 = self.get_feature(layer2)

        geom = self.get_geometry(feat)
        geom2 = self.get_geometry(feat2)

        geom = geom.Buffer(-1000)
        geom2 = geom2.Buffer(-1000)

        if geom.Intersects(geom2):
            intersection = geom.Intersection(geom2)
            return intersection, layer.GetSpatialRef()

        raise ValueError("Geometries doesn't intersects")

    def call_shell(self, command):
        """
        Void Method to call subprocess check_call command
        This method will call a shell subprocess and execute command

        Will raise a exception if an error ocurred during process

        Arguments:
            * command (str): str command to execute
        """
        subprocess.check_call(command, shell=True)

    def transform_raster_to_shapefile(
        self,
        input_file_path,
        output_path,
        data_name=1
    ):
        """
        Method to transform raster data to a mask shapefile

        Creates a shapefile mask from a raster useful area
        Translate mask to a virtual one band data
        Polygonize useful area to get common area between rasters

        Will return false if a exception or error ocurred during process

        Arguments:
            * input_file_path (str): input file path
            * output_path (str): output temp path to process
            * data_name (str): temporary data_name for layers
        Returns:
            * shapefile (str): shapefile for final data created
                Will be a polygon shapefile path
        """

        mask_file = "mask_{}.vrt".format(data_name)
        mask_file = os.path.join(output_path, mask_file)
        translated_file = "translated_{}.vrt".format(data_name)
        translated_file = os.path.join(output_path, translated_file)
        polygonize_file = "poly_{}.shp".format(data_name)
        polygonize_file = os.path.join(output_path, polygonize_file)

        mask = self.__get_mask_command()
        translated = self.__get_translated_data_command()
        polygonize = self.__get_polygonize_command()

        mask_command = mask.format(
            nodata=self.nodata, tiffile=input_file_path, output=mask_file)
        translate_command = translated.format(
            input=mask_file, output=translated_file)
        polygonize_command = polygonize.format(
            input=translated_file, output=polygonize_file)

        try:
            self.call_shell(mask_command)
            self.call_shell(translate_command)
            self.call_shell(polygonize_command)
        except Exception as exc:
            print(exc)
            return False

        return polygonize_file

    def createShapefile(self, geometry_data, filename, srs):
        """
        Method to create shapefile for intersected data

        Creates a shapefile data for geometry

        Arguments:
            * geometry_data (ogr.Geometry): Geometry data source
            * filename (str): output filename
            * srs (osr.SpatialRef): osr input spatial Reference
        Returns:
            * filename (str): output filename
        """

        try:
            driver = self.get_driver()
            if not filename.endswith("shp"):
                filename = "{}.shp".format(filename)
            data_source = driver.CreateDataSource(filename)

            layer = data_source.CreateLayer(
                "intersection", srs, ogr.wkbMultiPolygon)
            for feat in geometry_data:
                if feat.GetGeometryName() in ["POLYGON", "MULTIPOLYGON"]:
                    feature = ogr.Feature(layer.GetLayerDefn())
                    feature.SetGeometry(feat)
                    layer.CreateFeature(feature)
                    feature = None
                if feat.GetGeometryName() in ["LINEARRING"]:
                    geom = ogr.Geometry(ogr.wkbPolygon)
                    geom.AddGeometry(feat)
                    feature = ogr.Feature(layer.GetLayerDefn())
                    feature.SetGeometry(geom)
                    layer.CreateFeature(feature)
                    feature = None
                else:
                    print("ERROR - Geometry type is not recognized.")
            return filename
        except Exception as exc:
            raise ValueError(exc)

        return None

    def cut_images(
        self,
        red_band1,
        red_band2,
        near_infra_band1,
        near_infra_band2,
        input_path,
        intersection_path,
        cutted_path,
        intersection_file_name
    ):
        """
        Void Method to cut all the input images based on the intersection
        shapefile, building only images with commom area between all bands
        This method will call multiple shell command using GDAL (gdalwarp)

        Arguments:
            * r1 (str): Red band file path of the first image
            * r2 (str): Red band file path of the second image
            * n1 (str): Near infrared band file path of the first image
            * n2 (str): Near infrared band file path of the second image
            * intersection_path: path of the intersection shapefile
            * cutted_path: output path for the cutted images
        """

        cut_command = " -cutline " \
            + os.path.join(
                intersection_path, intersection_file_name + ".shp") \
            + " -crop_to_cutline -dstalpha "

        cut_commands = "gdalwarp " + os.path.join(
                input_path, red_band1) \
            + cut_command + os.path.join(cutted_path, "r1.tif") \
            + " && " + "gdalwarp " + os.path.join(
                input_path, red_band2) \
            + cut_command + os.path.join(cutted_path, "r2.tif") \
            + " && " + "gdalwarp " + os.path.join(
                input_path, near_infra_band1) \
            + cut_command + os.path.join(cutted_path, "n1.tif") \
            + " && " + "gdalwarp " + os.path.join(
                input_path, near_infra_band2) \
            + cut_command + os.path.join(cutted_path, "n2.tif")

        try:
            self.call_shell(cut_commands)
        except Exception as exc:
            print(exc)
            return False

    def transform_rasters(self):
        """
        Main method to transform rasters to shapefile mask and create
        a output shapefile with intersected data

        Returns:
            * filename (str): output filename
        """
        tempdir = tempfile.TemporaryDirectory(suffix="_shp_area")
        shapefile = self.transform_raster_to_shapefile(
            self.input_file_path, tempdir.name, data_name=1)
        shapefile2 = self.transform_raster_to_shapefile(
            self.input_file_path2, tempdir.name, data_name=2)

        if shapefile and shapefile2:
            try:
                intersection, srs = self.get_intersection(
                    shapefile, shapefile2)
                filename = self.createShapefile(
                    intersection, self.output_file_name, srs)
            except Exception as exc:
                print(exc)

        return filename or None


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument(
        "-r1",
        "--raster-1",
        dest="raster_1",
        required=True,
        help="Path to first image",
        metavar="RASTER_1"
    )
    parser.add_argument(
        "-r2",
        "--raster-2",
        dest="raster_2",
        required=True,
        help="Path to second image",
        metavar="RASTER_2"
    )
    parser.add_argument(
        "-nodata",
        "--nodata",
        dest="nodata",
        required=True,
        help="Nodata values comma separated",
        metavar="nodata"
    )
    parser.add_argument(
        "-o",
        "--output",
        dest="output",
        required=True,
        help="output file",
        metavar="OUTPUT_FILE"
    )

    args = parser.parse_args()
    area = ShapefileArea(
        args.raster_1, args.raster_2, args.nodata.split(","), args.output)
    area.transform_rasters()
