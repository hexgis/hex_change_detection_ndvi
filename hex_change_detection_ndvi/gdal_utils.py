import os
import subprocess
import tempfile
import numpy as np

from osgeo import ogr, osr
from shapely.geometry import Polygon
from shapely.ops import cascaded_union


MIN_AREA_THRESHOLD = os.getenv("MIN_AREA_THRESHOLD", 5000)
MAX_AREA_THRESHOLD = os.getenv("MAX_AREA_THRESHOLD", 50000000)


class GdalUtils:
    """ Gdal utils for ndvi process """

    @staticmethod
    def get_temp_output_directory():
        """Private method to return temporary output dir"""
        output_path = tempfile.TemporaryDirectory(suffix='_gdalUtils')
        return output_path.name

    @staticmethod
    def check_dir_exists(directory):
        """
        Check and creates directory on filesystem

        Arguments:
            * directory (str): directory path

        Returns:
            * directory (str): directory path
        """
        if not os.path.isdir(directory):
            os.makedirs(directory)
        return directory

    @staticmethod
    def check_geo_transform(band_1, band_2):
        """
        Method to validate geotransform equality for red and
        near_infrared bands

        Arguments:
            * band_1 (GdalBand): Red dataSrc
            * band_2 (GdalBand): Near Infrared dataSrc
        Returns:
            * data (Boolean): geo transform validation
        """
        return band_1.GetGeoTransform() == band_2.GetGeoTransform()

    @staticmethod
    def get_raster_band_as_array(src_image, band=1, quiet=False):
        """
        Method to get band from image according to band attribute

        Arguments:
            * src_image (GdalImage): Gdal image src
            * band (Int): Number of band from image, starts with 1
            * quiet (Boolean): verbose logs

        Returns:
            * srcband (GdalBand): gdal src data
        """

        if not quiet:
            print("Reading Raster as array from band...")
        return src_image.GetRasterBand(band).ReadAsArray().astype(np.float32)

    @staticmethod
    def get_projection(src_image, quiet=False):
        """
        Method to get projection from Source Image

        Arguments:
            * src_image (GdalBand): Gdal image src
            * quiet (Boolean): run quietly

        Returns:
            * geoTransform (GdalProjection): gdal src projection
        """

        if not quiet:
            print("Getting Projection from band.\n")
        return src_image.GetProjection()

    @staticmethod
    def get_geo_transform(src_image, quiet=False):
        """
        Method to get projection from Source Image

        Arguments:
            * src_image (GdalBand): Gdal image src
            * quiet (Boolean): verbose logs

        Returns:
            * geoTransform (GdalTransform): gdal src transformation
        """

        if not quiet:
            print("Getting GeoTransform from band.\n")

        return src_image.GetGeoTransform()

    @staticmethod
    def check_array_and_transformed_data(self, red, near_infrared):
        """
        Method to validate array size and geo transform for bands

        Arguments:
            * red (GdalBand): Red dataSrc
            * near_infrared (GdalBand): Near Infrared dataSrc
        Returns:
            * data (Boolean): geo transform validation
        """
        return GdalUtils.check_array_size(red, near_infrared) and \
            GdalUtils.check_geo_transform(red, near_infrared)

    @staticmethod
    def check_array_size(band_1, band_2):
        """
        Method to validate array size equality for red and nir bands

        Arguments:
            * band_1 (GdalBand): Band 1 Gdal dataSrc
            * band_2 (GdalBand): Band 2 Gdal dataSrc
        Returns:
            * equals (Boolean): array equality
        """
        return band_1.shape == band_2.shape

    @staticmethod
    def cutline(shapefile, input_raster, output_dir=None, output_name=None):
        """
        Cut a raster with shapefile and returns output raster created

        Will check if files exists on filesystem
        and creates temporary files if input files does not exists

        Raises:
            * ValueError in case of input file does not exists
            * ValueError for check_call subprocess

        Arguments:
            * shapefile (str): path to input shapefile
            * input_raster (str): path to input file raster
            * output_dir (str): output path where to save files
            * output_name (str): output name to raster

        Returns:
            * file (str): output path to file
        """

        command = "gdalwarp {input} -cutline {shp} " \
            "-crop_to_cutline -dstalpha {output}"

        if not os.path.exists(input_raster):
            raise ValueError("Input raster doesnt exists")

        if not os.path.exists(shapefile):
            raise ValueError("Shapefile path doesnt exists")

        if not output_name:
            output_name = next(tempfile._get_candidate_names()) + ".TIF"

        if not output_dir:
            output_dir = tempfile.TemporaryDirectory(suffix='_gdalUtils')
            output_dir = output_dir.name

        output_dir = GdalUtils.check_dir_exists(output_dir)
        outputPath = os.path.join(output_dir, output_name)

        command = command.format(
            shp=shapefile,
            input=input_raster,
            output=outputPath
        )
        subprocess.check_call(command, shell=True)

        return outputPath

    @staticmethod
    def create_polygons_from_lines(
        shapefile,
        output_file_name,
        output_path=None,
        cutline=0,
        min_area_threshold=MIN_AREA_THRESHOLD,
        max_area_threshold=MAX_AREA_THRESHOLD,
    ):
        """
        Creates a polygon shapefile from lines shapefile
        using cutline to remove unused data.

        Created polygons will be processed to remove unions and
        noise (areas below the threshold) from the output.

        Arguments:
            * shapefile (str): path to shapefile
            * output_file_name (str): polygons shapefile output filename
            * output_path (str): output dir to save files
            * cutline (int): cutline data for
            * min_area_threshold (float): minimum polygon area threshold
            * max_area_threshold (float): maximum polygon area threshold

        Returns:
            * file (str): output path to file
        """

        if not os.path.exists(shapefile):
            raise ValueError("Shapefile path doesnt exists")
        print("Processing shapefile from {}".format(shapefile))

        if not output_path:
            output_path = GdalUtils.get_temp_output_directory()

        output_path = GdalUtils.check_dir_exists(output_path)
        output_path = os.path.join(output_path, output_file_name)

        driver = ogr.GetDriverByName("ESRI Shapefile")
        ds_polygons = driver.CreateDataSource(output_path)
        ds_lines = driver.Open(shapefile, 0)
        ds_layer = ds_lines.GetLayer()

        srs = ds_lines.GetLayer().GetSpatialRef()
        output_layer = ds_polygons.CreateLayer("polygons", srs, ogr.wkbPolygon)
        # output_layer.CreateField(ogr.FieldDefn("change", ogr.OFTReal))

        area_field = ogr.FieldDefn("area", ogr.OFTReal)
        area_field.SetWidth(32)
        area_field.SetPrecision(5)
        output_layer.CreateField(area_field)

        polygons = []
        features_dissolved = []

        for feat in range(ds_layer.GetFeatureCount()):
            feat = ds_layer.GetFeature(feat)
            line_geometry = feat.geometry()

            if feat.GetField("change") < cutline:
                if(feat.GetField("change") < 0):
                    point_count = line_geometry.GetPointCount()
                    points = line_geometry.GetPoints()
                    first_point = points[0]
                    last_point = points[point_count-1]

                    if(first_point != last_point):
                        line_geometry.AddPoint_2D(
                            first_point[0],
                            first_point[1]
                        )

                    polygon = Polygon(line_geometry.GetPoints())

                    if polygon.area and \
                       polygon.area > min_area_threshold and \
                       polygon.area < max_area_threshold:
                        polygons.append(polygon)

        polygons_dissolved = cascaded_union(polygons)
        feature_geometry = ogr.CreateGeometryFromWkb(polygons_dissolved.wkb)

        target = osr.SpatialReference()
        target.ImportFromEPSG(4326)
        transform = osr.CoordinateTransformation(srs, target)

        for feature in feature_geometry:
            new_feature = feature.Clone()
            new_feature.Transform(transform)
            features_dissolved.append(new_feature.ExportToWkt())
            output_feature = ogr.Feature(output_layer.GetLayerDefn())
            output_feature.SetGeometry(feature)
            output_feature.SetField("area", feature.GetArea())
            output_layer.CreateFeature(output_feature)
            output_feature.Destroy()

        return features_dissolved
