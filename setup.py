#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    'numpy>=1.13.3',
    'requests>=2.18.4',
    'Shapely~=1.7.0',
]

setup_requirements = ['pytest-runner', 'gdal>=2.4', ]

test_requirements = ['pytest>=3', 'homura>=0.1.5', ]

setup(
    author="Hex Informatica LTDA",
    author_email='contato@hexgis.com',
    python_requires='>=3.5',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="A python package to detect changes using ndvicomposition",
    entry_points={
        'console_scripts': [
            'hex_change_detection_ndvi=hex_change_detection_ndvi.cli:main',
        ],
    },
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='hex_change_detection_ndvi',
    name='hex_change_detection_ndvi',
    packages=find_packages(include=['hex_change_detection_ndvi', 'hex_change_detection_ndvi.*']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/hexgis/hex_change_detection_ndvi',
    version='0.1.16',
    zip_safe=False,
)
