.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Hex Change Detection NDVI, run this command in your terminal:

.. code-block:: console

    $ pip install hex_change_detection_ndvi

This is the preferred method to install Hex Change Detection NDVI, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Hex Change Detection NDVI can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://github.com/hexgis/hex_change_detection_ndvi

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://github.com/hexgis/hex_change_detection_ndvi/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Github repo: https://github.com/hexgis/hex_change_detection_ndvi
.. _tarball: https://github.com/hexgis/hex_change_detection_ndvi/tarball/master
