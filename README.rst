=========================
Hex Change Detection NDVI
=========================


.. image:: https://img.shields.io/pypi/v/hex_change_detection_ndvi.svg
        :target: https://pypi.python.org/pypi/hex_change_detection_ndvi

.. image:: https://img.shields.io/travis/hexgis/hex_change_detection_ndvi.svg
        :target: https://travis-ci.com/hexgis/hex_change_detection_ndvi

.. image:: https://readthedocs.org/projects/hex-change-detection-ndvi/badge/?version=latest
        :target: https://hex-change-detection-ndvi.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




A python package to detect changes using ndvicomposition


* Free software: GNU General Public License v3
* Documentation: https://hex-change-detection-ndvi.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
